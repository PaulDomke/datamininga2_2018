import csv

#These list will hold the amount of classes failed and missed and avg alchohol consumption
failuresList=[]
absencesList=[]
AlcList=[]

#I took the data of both weekend and weekday to find the average of the total they drink in a week for less dimensions
avgAlcList=[]

counter = 0

with open('student-por.CSV', encoding="utf8") as file:
    fileReader = csv.reader(file)
    for row in fileReader:
        #Ignores first row
        if counter == 0:
            counter = counter + 1
        #Takes all the data we need out
        else:
            failuresList.insert(counter - 1, row[14])
            absencesList.insert(counter - 1, row[29])
            AlcList.insert(counter - 1, (float(row[26]) + float(row[25]))/2)

finalList=[]
finalListSmall=[]

#this creates a list of list to help things go into the csv file
for i in range(len(AlcList)):
    finalList.append([failuresList[i], absencesList[i], AlcList[i]])

for i in range(100):
    finalListSmall.append([failuresList[i], absencesList[i], AlcList[i]])

#Writes it to output
with open("output.csv", "w") as f:
    writer = csv.writer(f)
    writer.writerows(finalList)

with open("outputSmall.csv", "w") as f:
    writer = csv.writer(f)
    writer.writerows(finalListSmall)
