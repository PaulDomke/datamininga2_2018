---
title: "Data Mining Assignment 2"
author: "Courtney Miller"
date: "10/5/2018"
output: html_document
---

## Determing Appropriate K for K means and K mediods Clustering 
```{r setup, include=FALSE}
library(Lock5Data)
library(Lock5withR)
library(mosaic)
library(ggplot2)
library(mosaicData)
library(leaflet)
library(dplyr)
library(readr)

data <- read_csv("~/Desktop/Data Mining/A2/datamininga2_2018/data/OldWimpyData/output.csv")

knitr::opts_chunk$set(echo = TRUE)
```

```{r}
d = NULL
findingK <- function(data){
  # According to rule of thumb from class this is "enough" K but probably too much
  maxK <- sqrt(nrow(data)/2)
  
  for (i in 1 : maxK) {
    
    k2 <- kmeans(data, i)
    intra <- sum(k2$withinss)
    inter <- sum(k2$betweenss)
    interIntra <- inter/intra
    
    d = rbind(d, data.frame(i, interIntra))
  } 
  
  View(d)
}

```

```{r}
scaled_data = as.matrix(scale(data))

k.max <- 18
data1 <- scaled_data
wss <- sapply(1:k.max, 
              function(k){kmeans(data1, k, nstart=50,iter.max = 15)$tot.withinss})
wss
plot(1:k.max, wss,
     type="b", pch = 19, frame = FALSE, 
     xlab="Number of clusters K",
     ylab="Total within-clusters sum of squares")
```


