---
title: "Assignment 2 Visualizations"
author: "Courtney Miller"
date: "10/7/2018"
output: html_document
---

```{r setup, include=FALSE}
library(Lock5Data)
library(Lock5withR)
library(mosaic)
library(ggplot2)
library(mosaicData)
library(leaflet)
library(dplyr)
library(readr)
library(scatterplot3d)

data <- read_csv("~/Desktop/Data Mining/A2/datamininga2_2018/data/Datasets/output.csv", col_names = FALSE)
kMeansData <- read_csv("~/Desktop/Data Mining/A2/datamininga2_2018/data/KMeansData.csv", col_names = FALSE)
kMedoidsData <- read_csv("~/Desktop/Data Mining/A2/datamininga2_2018/data/KMedoidsData.csv", col_names = FALSE)


knitr::opts_chunk$set(echo = TRUE)
```

## K Means:

```{r}
#colors <- c("#999999", "#E69F00", "#56B4E9", "#FDDBC7")
#colors <- colors[kMeansData$X1]

p <- scatterplot3d(kMeansData[,2:4], xlab = "# of Failures", ylab = "# of Absences", zlab = "Avg. Alcohol Consumpton", angle = 60, color=ifelse(kMeansData$X1 %in% 0, "#DE6449", ifelse(kMeansData$X1 %in% 1,"#531CB3", ifelse(kMeansData$X1 %in% 2, "#000000", ifelse(kMeansData$X1 %in% 3, "#23CE6B", "#FF9914")))))


```

## K Mediods:

```{r}

p <- scatterplot3d(kMedoidsData[,2:4], xlab = "# of Failures", ylab = "# of Absences", zlab = "Avg. Alcohol Consumpton", angle = 60, color=ifelse(kMedoidsData$X1 %in% 0, "#DE6449", ifelse(kMedoidsData$X1 %in% 1,"#531CB3", ifelse(kMedoidsData$X1 %in% 2,"#000000", ifelse(kMeansData$X1 %in% 3, "#23CE6B", "#FF9914")))))


```

## Hierarchical Clustering:

```{r}
clusters <- hclust(dist(data[,1:3]), method = 'average')
plot(clusters)
rect.hclust(clusters, k = 5, border = 2:6)
abline(h = 5, col = 'red')

clusterCut <- cutree(clusters, 4)
table(clusterCut, kMeansData$X1)







#ggplot(kMeansData, aes(X3, X4, color=as.factor(kMeansData$X1))) + geom_point(alpha = 0.4, size = 3.5) + geom_point(col= #as.factor(clusterCut)) + scale_color_manual(values = c("#DE6449", "#531CB3", "#000000", "#23CE6B"))

#scatterplot3d(kMedoidsData[,2:4], xlab = "# of Failures", ylab = "# of Absences", zlab = "Avg. Alcohol Consumpton", angle = 60, #color=ifelse(clusterCut %in% 1, "#DE6449", ifelse(clusterCut %in% 2,"#531CB3", ifelse(clusterCut %in% 3, "#000000","#23CE6B" ))))

```