Link to dataset:
https://www.kaggle.com/uciml/student-alcohol-consumption#student-por.csv

Both kmeans.py and kmedoids.py take in two inputs: first the dataset, then the number of clusters desired.

To run kmeans.py, in the terminal go to the src directory and then enter the following:
python kmeans.py "../data/OldWimpyData/output.csv" 5

To run kmedoids.py, in the terminal go to the src directory and then enter the following:
python kmedoids.py "../data/Datasets/output.csv" 5

kmeans.py's output is saved to KMeansData.csv
kmedoids.py's output is saved to KMedoidsData.csv


Hclust takes in 1 input, the dataset.

To run hclust, in the terminal go to the src directory and then enter the following:
python hclust.py "../data/Datasets/output.csv"

hclust.py's output is saved to HClustData.json