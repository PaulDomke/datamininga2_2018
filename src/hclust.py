#A template for the implementation of hclust
import math #sqrt
import sys
import csv
import json


# Accepts two data points a and b.
# Returns the distance between a and b.
# Note that this might be specific to your data.
def Distance(a,b):
	dist = 0
	# iterate through both points at the same time
	for i, j, in zip(a, b):
		# add each feature's difference together
		dist = dist + abs(i - j)
	return dist


# Accepts two data points a and b.
# Produces a point that is the average of a and b.
def merge(a,b):
	point = []
	# iterate through both points at the same time
	for i, j, in zip(a, b):
		#calculate the average of each feature
		temp = (i + j) / 2
		point.append(temp)
	return point


# Accepts a list of data points.
# Returns the pair of points that are closest
def findClosestPair(D):
	# start with some bad guesses on what the closest points are
	closest = [D[0], D[1]]
	closestDist = Distance(D[0], D[1])
	# compare every possible pair of points
	for point1 in D:
		for point2 in D:
			if point1 != point2:
				dist = Distance(point1, point2)
				# compare their dist to the current closest dist
				if dist < closestDist:
					closestDist = dist
					closest = [point1, point2]
	return closest


# Accepts a list of data points.
# Produces a tree structure corresponding to a
# Agglomerative Hierarchal clustering of D.
def HClust(D):
	# start with all of the points as possible clusters
	centers = D
	# dictionary for tree to be placed in
	splits = {}
	# while there are points left to be compared
	while len(centers) > 1:
		# find the two points closest to one another
		location = findClosestPair(centers)
		# remove one point
		centers.remove(location[1])
		# merge the two points and place them in the index of the other point
		index = centers.index(location[0])
		centers[index] = merge(location[0], location[1])
		# string versions of each point
		strLoc0 = str(location[0])
		strLoc1 = str(location[1])
		# if both are in the tree already, use their dictionary values
		if strLoc0 in splits and strLoc1 in splits:
			splits[str(centers[index])] = {strLoc0: splits[strLoc0], strLoc1: splits[strLoc1]}
			# remove their duplicates
			splits.pop(strLoc0)
			splits.pop(strLoc1)
		# else if the first point is in the tree already, use its dictionary value
		elif strLoc0 in splits:
			splits[str(centers[index])] = {strLoc0: splits[strLoc0], strLoc1: "original pt"}
			# remove its duplicate
			splits.pop(strLoc0)
		# else if the second point is in the tree already, use its dictionary value
		elif strLoc1 in splits:
			splits[str(centers[index])] = {strLoc0: "original pt", strLoc1: splits[strLoc1]}
			# remove its duplicate
			splits.pop(strLoc1)
		else:
			splits[str(centers[index])] = {strLoc0: "original pt", strLoc1: "original pt"}
	return splits


# open data to process
with open(sys.argv[1], encoding="utf8") as file:
    fileReader = csv.reader(file)
    data = []
    for row in fileReader:
        temp = []
        for i in row:
            temp.append(float(i))
        data.append(temp)
    for point in data:
        if point == []:
            data.remove(point)

output = HClust(data)

with open('../data/HClustData.json', 'w') as file:
    json.dump(output, file)




# print(HClust([[4,4], [4,3], [3,3], [7,3], [8,3], [7,1], [5,10], [5,8], [4,8], [4,7], [3,4], [7,2], [5,9]]))

####
#
# Distance Tests
#
####

# # [1,1], [2,2] -> 2
# print(Distance([1,1], [2,2]))

# # [5,2], [3,7] -> 7
# print(Distance([5,2], [3,7]))


####
#
# Merge Tests
#
####

# # [1,1], [2,2] -> [1.5,1.5]
# print(merge([1,1], [2,2]))

# # [5,2], [3,7] -> [4,4.5]
# print(merge([5,2], [3,7]))


####
#
# findClosestPair Tests
#
####

# # [1,1], [3,5], [7,7], [2,2] -> [1,1], [2,2]
# print(findClosestPair([[1,1], [3,5], [7,7], [2,2]]))

# # [1,1], [3,5], [7,7], [3,9], [8,8] -> [7,7], [8,8]
# print(findClosestPair([[1,1], [3,5], [7,7], [3,9], [8,8]]))