#A template for the implementation of K-Medoids.
import math #sqrt
import csv
import sys

# Accepts two data points a and b.
# Returns the distance between a and b.
# Note that this might be specific to your data.
def Distance(a,b):
	dist = 0
	for i, j, in zip(a, b):
		dist = dist + abs(i - j)
	return dist


# Accepts a list of data points D, and a list of centers
# Returns a dictionary of lists called "clusters", such that
# clusters[c] is a list of every data point in D that is closest
#  to center c.
# Note: This can be identical to the K-Means function of the same name.
def assignClusters(D, centers):
	# empty dictionary -- to be returned
    clusters = {}
    # iterate through centers and give each one a value of an empty list
    for center in centers:
        clusters[tuple(center)] = []
    # iterate through each data point in D
    for point in D:
        # empty dictionary to store the distances from the data point to each center
        centerDist = {}
        # iterate through each center
        for center in centers:
            # calculate euclidian distance from the data point to center
            dist = Distance(point, center)
            # store distance as value for center key
            centerDist[tuple(center)] = dist
        # starter center to assign point to -- 1st center by default
        bestCenter = list(centerDist.keys())[0]
        # iterate through keys and values
        for key, value in centerDist.items():
            # compare the dist values of each center
            # goal is to find the smallest distance
            if value < centerDist.get(bestCenter):
                bestCenter = key
        # append point to center in which it had the shortest distance to
        clusters.get(tuple(bestCenter)).append(point)
    return clusters

# Accepts a list of data points.
# Returns the mean of the points.
def findClusterMean(cluster):
    # what we will be dividing each sum with after adding each feature for each point together
    n = len(cluster)
    # what we are returning -- an avg point between all points in cluster
    mean = []
    # iterate through range of number equal to number of features
    for num in range(len(cluster[0])):
        avg = 0
        # iterate through each data point
        for point in cluster:
            # avg is acting as sum of a feature of all points at first
            avg = avg + point[num]
        # divide by number of data points in cluster
        avg = avg / n
        # add feature avg to point we will be returning
        mean.append(avg)
    return mean

# Accepts a list of data points.
# Returns the medoid of the points.
def findClusterMedoid(cluster):
	# find mean point of the cluster
	mean = findClusterMean(cluster)
	# bad guess -- start with first point
	bestCenter = cluster[0]
	# compare each point's distance to the mean point
	for point in cluster:
		if Distance(point, mean) < Distance(bestCenter, mean):
			bestCenter = point
	# return the point that has the smallest distance to the mean point
	return bestCenter


# Accepts a list of data points, and a number of clusters.
# Produces a set of lists representing a K-Medoids clustering
#  of D.
def KMedoids(D, k):
	# start with a bad guess
	centers = D[0:k]
	oldCenters = None
	# while the centers are changing each loop
	while centers != oldCenters:
		# keep track of previous centers
		oldCenters = centers
		# find clusters
		clusters = assignClusters(D, centers)
		# find (possibly) new center point
		centers = []
		for center in clusters:
			centers.append(findClusterMedoid(clusters[center]))
	# return final clusters once the centers of each cluster no longer changes in loop
	return clusters


# open data to process
with open(sys.argv[1], encoding="utf8") as file:
    fileReader = csv.reader(file)
    data = []
    for row in fileReader:
        temp = []
        for i in row:
            temp.append(float(i))
        data.append(temp)
    for point in data:
        if point == []:
            data.remove(point)

# run kmedoids on data
output = KMedoids(data, int(sys.argv[2]))

# write results to KMedoidsData.csv
with open("../data/KMedoidsData.csv", "w") as f:
    writer = csv.writer(f)
    group = 0
    for center in output:
        for point in output[center]:
            point.insert(0, group)
            writer.writerow(point)
        group = group + 1




####
#
# Distance Tests
#
####

# # [1,1], [2,2] -> 2
# print(Distance([1,1], [2,2]))

# # [5,2], [3,7] -> 7
# print(Distance([5,2], [3,7]))


####
#
# assignClusters tested in kmeans.py
#
####


####
#
# findClusterMean tested in kmeans.py
#
####

####
#
# findClusterMedoid Tests
#
####

# print(KMedoids([[4,4], [4,3], [3,3], [7,3], [8,3], [7,1], [5,10], [5,8], [4,8], [4,7], [3,4], [7,2], [5,9]], 3))

# print(findClusterMedoid([[5,10],[5,9],[5,8],[4,8],[4,7],[6,10]]))