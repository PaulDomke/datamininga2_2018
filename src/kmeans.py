#A template for the implementation of K-Means.
import math #sqrt
import csv
import sys

# Accepts two data points a and b.
# Returns the euclidian distance
# between a and b.
def euclidianDistance(a,b):
    # works for multi-dimensional data
    # number to be placed in sqrt
    temp = 0
    # iterate through both points at the same time
    for i, j in zip(a, b):
        # calculate sums inside sqrt
        temp = temp + ((i - j) ** 2)
    # sqrt sums to get final euclidian distance
    dist = math.sqrt(temp)
    return dist


# Accepts a list of data points D, and a list of centers
# Returns a dictionary of lists called "clusters", such that
# clusters[c] is a list of every data point in D that is closest
#  to center c.
def assignClusters(D, centers):
    # empty dictionary -- to be returned
    clusters = {}
    # iterate through centers and give each one a value of an empty list
    for center in centers:
        clusters[tuple(center)] = []
    # iterate through each data point in D
    for point in D:
        # empty dictionary to store the distances from the data point to each center
        centerDist = {}
        # iterate through each center
        for center in centers:
            # calculate euclidian distance from the data point to center
            dist = euclidianDistance(point, center)
            # store distance as value for center key
            centerDist[tuple(center)] = dist
        # starter center to assign point to -- 1st center by default
        bestCenter = list(centerDist.keys())[0]
        # iterate through keys and values
        for key, value in centerDist.items():
            # compare the dist values of each center
            # goal is to find the smallest distance
            if value < centerDist.get(bestCenter):
                bestCenter = key
        # append point to center in which it had the shortest distance to
        clusters.get(tuple(bestCenter)).append(point)
    return clusters


# Accepts a list of data points.
# Returns the mean of the points.
def findClusterMean(cluster):
    # what we will be dividing each sum with after adding each feature for each point together
    n = len(cluster)
    # what we are returning -- an avg point between all points in cluster
    mean = []
    # iterate through range of number equal to number of features
    for num in range(len(cluster[0])):
        avg = 0
        # iterate through each data point
        for point in cluster:
            # avg is acting as sum of a feature of all points at first
            avg = avg + point[num]
        # divide by number of data points in cluster
        avg = avg / n
        # add feature avg to point we will be returning
        mean.append(avg)
    return mean
            

# Accepts a list of data points, and a number of clusters.
# Produces a set of lists representing a K-Means clustering
#  of D.
def KMeans(D, k):
    # start with a bad guess
    means = D[0:k]
    oldMeans = None
    # while the means / clusters are changing each loop
    while means != oldMeans:
        # keep track of the previous means
        oldMeans = means
        # find clusters
        clusters = assignClusters(D, means)
        # find (possibly) new means
        means = []
        for center in clusters:
            means.append(findClusterMean(list(clusters[center])))
    # return final clusters once means no longer changes in loop
    return clusters
        

# open data to process
with open(sys.argv[1], encoding="utf8") as file:
    fileReader = csv.reader(file)
    data = []
    for row in fileReader:
        temp = []
        for i in row:
            temp.append(float(i))
        data.append(temp)
    for point in data:
        if point == []:
            data.remove(point)

# run kmeans on data
output = KMeans(data, int(sys.argv[2]))

# write results to KMeansData.csv
with open("../data/KMeansData.csv", "w") as f:
    writer = csv.writer(f)
    group = 0
    for center in output:
        for point in output[center]:
            point.insert(0, group)
            writer.writerow(point)
        group = group + 1


####
#
# euclidianDistance Tests
#
####

# # [1,1], [2,2] -> 1.4142
# print(euclidianDistance([1,1], [2,2]))

# # [3,5], [2,8] -> 3.1623
# print(euclidianDistance([3,5], [2,8]))


####
#
# assignClusters Test
#
####
    
# # D = [4,4], [4,3], [3,3], [7,3], [8,3], [7,1], [5,10], [5,8], [4,8], [4,7]
# # centers = [3,4], [7,2], [5,9]
# # ->
# # {(3,4) : [[4,4], [3,3], [4,3]],
# #  (7,2) : [[7,3], [8,3], [7,1]],
# #  (5,9) : [[5,10], [5,8], [4,8], [4,7]]}
# print(assignClusters([[4,4], [4,3], [3,3], [7,3], [8,3], [7,1], [5,10], [5,8], [4,8], [4,7]], [[3,4], [7,2], [5,9]]))


####
#
# findClusterMean Tests
#
####

# # [1,1],[2,2],[3,3] -> [2,2]
# print(findClusterMean([[1,1],[2,2],[3,3]]))

# # [1,1], [5,7], [8,8] -> [4.6667, 5.3333]
# print(findClusterMean([[1,1], [5,7], [8,8]]))


# print(KMeans([[4,4], [4,3], [3,3], [7,3], [8,3], [7,1], [5,10], [5,8], [4,8], [4,7], [3,4], [7,2], [5,9]], 3))

        
